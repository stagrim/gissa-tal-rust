// imports the io module from the standard library
use std::io;
// imports the modules needed for the rand library to work
use rand::prelude::*;

fn main() {
    // let declares a variable, mut makes it mutable, meaning you can change its value later
    let mut input;
    let number: u32 = rand::thread_rng().gen_range(1, 101);
    // [u32; 2] means: create an array that will contain 2, u32(unsigned 32 bits/4 bytes) values
    let mut guess: [u32; 2];
    let mut tries: u32 = 0;

    // Cheating during debugging
    // println!("{}", number);

    loop {
        // since input needs the be reset each iteration I decided to put it's value to String::new() here instead of when it is declared to avoid repeating myself
        input = String::new();

        println!("Enter a number between 1 to 100");
        // Read the number given by the user
        io::stdin().read_line(&mut input)
        // Error message in case the program can't read from the console, rust priorities safe code, where all potential crash points are covered
        // also it is common to break up long lines to improve readability in rust "two lines for two method calls"
            .expect("Failed to read line");
        // give guess the returned value of exp() with input.trim() as a parameter
        // trim() removes white spaces captured by read_line(): "4\n" -> "4"
        guess = exp(input.trim());

        // Handles inputs other than 0 < guess < 101 and prints out an appropriate error message
        if guess[0] == 0 && guess[1] == 0 {
            println!("The number can't contain letters, please try again\n");
            continue;
        }
        else if guess[1] == 0 || guess[0] < 1 || guess[0] > 100{
            println!("Must be a number between 1 to 100, please try again\n");
            continue;
        }

        // An illegal input doesn't result in a try, so this is put after the previous if statement
        tries = tries + 1;

        if guess[0] == number {
            println!("Congratulations, you guessed {} correctly after {} tries", guess[0], tries);
            // Similar to Console.ReadLine(), but I'm pretty sure this is really bad rust code
            // .unwrap() handles potential errors
            io::stdin().read_line(&mut String::new()).unwrap();
            // Break the loop and finish the program
            break;
        }
        else if guess[0] > number {
            println!("You guessed too high\n");
        }
        else if guess[0] < number {
            println!("You guessed too low\n");
        }
    }

}

// Exception handling converting &str to an array containing [u32; 2] where [1] is 1 if successful and 0 if not
// If [1] is equal to 0 [0] becomes a 0 if it is a ParseIntError and 1 if it's another error
fn exp(input: &str) -> [u32; 2] {
    // Expectation handling from parse is unsafe and only available in the nightly build at the moment
    // This is a not so elegant solution
    // parse_int_error will, thanks to .err() contain the error message for ParseIntError which makes it possible to compare it to the error returned in the match block
    let parse_int_error = "abc".parse::<u32>().err().unwrap();
    // match is similar to switch() from C#
    // case Ok(`returns the successful result from input.parse() that I named n`): return [n, 1]
    // case Err(`returns error message that I named _, because I won't use it`): return [0, 0]
    // Numbers with decimals will also return an Err(_) since a f32/f64 can't be parsed into a u32
    let output: [u32; 2] = match input.parse() {
        // If successful, parse will return Result Ok(n) where n is the returned value, in this case input as a u32
        Ok(n) => [n, 1],
        // If the error is a ParseIntError
        Err(ref e) if *e == parse_int_error => [0, 0],
        // All other possible errors
        _ => [1, 0]
    };
    return output;
}